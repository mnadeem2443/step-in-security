
<div class="panel-group">
    <div class="panel panel-primary">
      <div class="panel-heading">Student Enter/Exit Record</div>
      <div class="panel-body">
	  
	  <form class="form-horizontal" role="form">
   
   <div class="form-group">
    <label class="control-label col-sm-2" for="name">Student ID:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" id="std_id" placeholder="Enter Student ID">
    </div>
  </div>
   
  <div class="form-group">
    <label class="control-label col-sm-2" for="name">Name:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" id="name" placeholder="Enter Name">
    </div>
  </div>
 
  <div class="form-group">
    <label class="control-label col-sm-2" for="father_name">Father Name:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" id="f_name" placeholder="Enter Father Name">
    </div>
  </div>
  
  
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="class">Class:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" id="class" placeholder="Enter Class">
    </div>
  </div>
  
   
  <div class="form-group">
    <label class="control-label col-sm-2" for="clas_roll">Class Roll No:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" id="c_roll" placeholder="Enter Address">
    </div>
  </div>
  
   <div class="form-group">
    <label class="control-label col-sm-2" for="clas_roll">School:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" id="school" placeholder="Enter School (e.g boys or girls)">
    </div>
  </div>
  
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="sal">Time In:</label>
    <div class="col-sm-10">
      <input type="Time" class="form-control" placeholder="Enter Time In">
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="sal">Time Out:</label>
    <div class="col-sm-10">
      <input type="Time" class="form-control" placeholder="Enter Time Out">
    </div>
  </div>

  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success">Add</button>
    </div>
  </div>
</form>
	  
	 


	  
	 
    </div>
	</div>
	</div>

