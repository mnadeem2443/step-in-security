
<div class="panel-group">
    <div class="panel panel-primary">
      <div class="panel-heading">School Staff Enter/Exit Record</div>
      <div class="panel-body">
	  
	  <form class="form-horizontal" role="form">
	  
	  <div class="form-group">
    <label class="control-label col-sm-2" for="name">Employee ID:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" id="emp_id" placeholder="Enter Employee ID">
    </div>
  </div>
	  
  <div class="form-group">
    <label class="control-label col-sm-2" for="name">Name:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" id="name" placeholder="Enter Name">
    </div>
  </div>
 
  <div class="form-group">
    <label class="control-label col-sm-2" for="father_name">Father Name:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" id="f_name" placeholder="Enter Father Name">
    </div>
  </div>
  

  
  <div class="form-group">
    <label class="control-label col-sm-2" for="des">Designation:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" placeholder="Enter Designation">
    </div>
  </div>
  
   
  <div class="form-group">
    <label class="control-label col-sm-2" for="adrs">School:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" placeholder="Enter School (e.g Boys or Girls)">
    </div>
  </div>
  
 <div class="form-group">
    <label class="control-label col-sm-2" for="des">Time In:</label>
    <div class="col-sm-10">
      <input type="time" class="form-control" placeholder="Enter Time In">
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="des">Time Out:</label>
    <div class="col-sm-10">
      <input type="time" class="form-control" placeholder="Enter Time Out">
    </div>
  </div>
  
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success">Add</button>
    </div>
  </div>
</form>
	  
	 


	  
	 
    </div>
	</div>
	</div>

