<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel='stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.12.0-1/css/all.min.css' />
    <link rel="stylesheet" href="css/style.css">
    <title>Forms</title>
</head>

<body>
    <?php
    if(isset($_POST['role']) && isset($_POST['username']))
    {
        $_SESSION['role'] = $_POST['role'];
        $_SESSION['username'] = $_POST['username'];
    }
    ?>
    <header>
        <main class="container flex flex-column items-center justify-center">
            <form class="flex justify-between" method="post" action="index.php">
                <div class="content flex flex-column justify-center items-center">
                    <div class="text flex flex-column justify-center items-center">
                        <h1>user login</h1>
                        <div class="icon">
                            <i class="fab fa-facebook"></i>
                            <i class="fab fa-twitter"></i>
                            <i class="fab fa-google"></i>
                        </div>
                        <span>or use social media to signup</span>
                    </div>
                    <div class="form-group">
                        <input type="text"  name="username" placeholder="username">
                    </div>
                    <div class="form-group">
                        <input type="text" name="role" placeholder="role">
                    </div>
                    <div class="form-group">
                        <input type="submit" value="login">
                    </div>
                </div>
                <aside class="flex flex-column justify-center items-center">
                    <h1>welcome, back!</h1>
                    <h2>by creating your account your are agree to our privacy and policy.</h2>
                    <button type="button">signup</button>
                </aside>
            </form>
        </main>
    </header>

</body>

</html>