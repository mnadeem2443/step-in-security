<?PHP session_start(); ?>
<html>
<head>
<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="css.css">

<!-- jQuery library -->
<script src="js.js"></script>

<!-- Latest compiled JavaScript -->
<script src="js1.js"></script> 

<link rel="stylesheet" media="screen,projection" type="text/css" href="css/main.css" />

<title>Step In Security</title>
</head>

<body>
<?php
if(!isset($_SESSION["username"]) && !isset($_SESSION["role"])){include "login.php"; }
else {

echo "<div class=container-fluid>";
include "header.php";
echo "<div class=col-sm-2>";
echo "<a href='/sit/?page_no=0'><img src='img/logo5.jpg' class='img-thumbnail panel panel-primary' alt='Go to Home Page' width=304 height=236></a>";
include "nav_bar.php";
 
echo "</div>";


echo "<div class=col-sm-10>";



if(isset ($_GET["page_no"]))
{
	$page=$_GET["page_no"];
	
	
	switch ($page) {
    case "1":
        include "emp_add.php";
        break;
    case "2":
        include "family_add.php";
        break;
    case "3":
        include "std_add.php";
        break;
	case "4":
        include "vhcl_add.php";
        break;
    case "5":
       include "entery_exit.php";
        break;
    case "6":
        include "s_stf_data.php";
        break;
	 case "7":
        include "daily_std_data.php";
        break;
	 case "8":
        include "gst_add.php";
        break;
	case "9":
        include "vhcl_enter.php";
        break;
	case "10":
        include "emp_data.php";
        break;
	 case "11":
        include "std_data.php";
        break;
	 case "12":
        include "family_data.php";
        break;
	case "13":
        include "gst_data.php";
        break;
	case "14":
        include "vhcl_data.php";
        break;
	case "15":
        include "app_data.php";
        break;
	case "16":
        include "active_passes.php";
        break;
	case "17":
        include "gst_enter.php";
        break;
	case "18":
        include "gst_exit.php";
        break;
		case "19":
        include "apply_night_stay.php";
        break;
		case "20":
        include "reports.php";
        break;
		case "21":
        include "vhcl_entery_exit.php";
        break;
		case "22":
        include "member_add.php";
        break;
		case "24":
        include "family_members_data.php";
        break;
		
    case "0":
        include "home.php";
        break;
}

}

else {
	include "home.php";
	
}

echo "</div></div>";

echo "<div class=col-sm-12 align = 'bottom'>";
include "footer.php";
echo "</div>";

}


 ?>


</body>
</html>