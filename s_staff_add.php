
<div class="panel-group">
    <div class="panel panel-primary">
      <div class="panel-heading">New School Staff</div>
      <div class="panel-body">
	  
	  <form class="form-horizontal" role="form">
  <div class="form-group">
    <label class="control-label col-sm-2" for="name">Name:</label>
    <div class="col-sm-10">
      <input type="name" class="form-control" id="name" placeholder="Enter Name">
    </div>
  </div>
 
  <div class="form-group">
    <label class="control-label col-sm-2" for="father_name">Father Name:</label>
    <div class="col-sm-10">
      <input type="father_name" class="form-control" id="f_name" placeholder="Enter Father Name">
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="father_name">CNIC#:</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" id="cnic" placeholder="Enter CNIC#">
    </div>
  </div>
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="dob">Date Of Birth:</label>
    <div class="col-sm-10">
      <input type="date" class="form-control" id="dob" placeholder="Enter date of birth">
    </div>
  </div>
  
  
  <div class="form-group">
    <label class="control-label col-sm-2" for="des">Designation:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" placeholder="Enter Designation">
    </div>
  </div>
  
   
  <div class="form-group">
    <label class="control-label col-sm-2" for="adrs">Address:</label>
    <div class="col-sm-10">
      <input type="string" class="form-control" placeholder="Enter Address">
    </div>
  </div>
  
   
  <div class="form-group">
    <label class="control-label col-sm-2" for="sal">Salary:</label>
    <div class="col-sm-10">
      <input type="number" class="form-control" placeholder="Enter Salary">
    </div>
  </div>
<div class="col-sm-offset-2 col-sm-10">  
<label class="control-label col-sm-2" for="gender">Gender:</label>
<label class="radio-inline"><input type="radio" name="optradio">Male</label>
<label class="radio-inline"><input type="radio" name="optradio">Female</label>
<label class="radio-inline"><input type="radio" name="optradio">Other</label>
</div>
  <div class="form-group"> 
    <div class="col-sm-offset-2 col-sm-10">
      <button type="submit" class="btn btn-success">Add</button>
    </div>
  </div>
</form>
	  
	 


	  
	 
    </div>
	</div>
	</div>

